from pymongo import MongoClient

from flask import g, current_app

def getDb():
    config = current_app.config

    if 'db' not in g:
        g.mongo = MongoClient(config['MONGO_URI'])
        g.db = g.mongo[config['MONGO_DB']]

    return g.db
    
def closeDB():
    mongo = g.pop('mongo')

    if mongo is not None:
        mongo.close()

def init_app(app):
    app.teardown_appcontext(closeDB)