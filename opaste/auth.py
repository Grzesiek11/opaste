import functools

from flask import Blueprint, request, redirect, url_for, flash, render_template, session, g, current_app

import werkzeug.security as security

from bson import ObjectId

from requests import post

from opaste.db import getDb

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        db = getDb()
        config = current_app.config

        name = request.form['name']
        password = request.form['password']

        error = None

        if not name:
            error = 'Username is required'
        elif not password:
            error = 'Password is required'
        
        elif db.users.find_one({'name': name}):
            error = 'Username is taken'
        
        if config['HCAPTHA_ENABLED']:
            hcapthaResponse = request.form['h-captcha-response']

            hcapthaVerify = post(config['HCAPTHA_VERIFY_URL'], data={'secret': config['HCAPTHA_SECRET'], 'sitekey': config['HCAPTHA_SITEKEY'], 'response': hcapthaResponse}).json()
            if not hcapthaVerify['success']:
                error = 'Captha verification failed'
        
        if error is None:
            db.users.insert_one({'name': name, 'password': security.generate_password_hash(password)})
            return redirect(url_for('auth.login'))
        else:
            flash(error)
    
    return render_template('auth/register.html')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']
        db = getDb()
        error = None
        user = db.users.find_one({'name': name})

        if user is None or not security.check_password_hash(user['password'], password):
            error = 'Invalid username or password'

        if error is None:
            session.clear()
            session['userID'] = str(user['_id'])
            return redirect(url_for('index'))
        else:
            flash(error)

    return render_template('auth/login.html')

@bp.before_app_request
def loadUser():
    userID = session.get('userID')

    if userID is None:
        g.user = None
    else:
        g.user = getDb().users.find_one({'_id': ObjectId(userID)})

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def loginRequired(view):
    @functools.wraps(view)
    def wrappedView(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)
    
    return wrappedView
