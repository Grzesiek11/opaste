import os

from flask import Flask, redirect, url_for

import pymongo

def create_app():
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_mapping({
        'SECRET_KEY': 'opaste',
        'MONGO_URI': 'mongodb://localhost',
        'MONGO_DB': 'opaste',
        'HCAPTHA_ENABLED': False,
        'HCAPTHA_VERIFY_URL': 'https://hcaptcha.com/siteverify'
    })

    os.makedirs(app.instance_path, exist_ok=True)

    app.config.from_json('config.json', silent=True)

    from . import auth, pastes, users
    app.register_blueprint(auth.bp)
    app.register_blueprint(pastes.bp)
    app.register_blueprint(users.bp)
    
    @app.route('/')
    def index():
        return redirect(url_for('pastes.new'))

    return app
